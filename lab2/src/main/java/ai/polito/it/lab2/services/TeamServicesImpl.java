package ai.polito.it.lab2.services;

import ai.polito.it.lab2.dtos.CourseDTO;
import ai.polito.it.lab2.dtos.StudentDTO;
import ai.polito.it.lab2.dtos.TeamDTO;
import ai.polito.it.lab2.entities.Course;
import ai.polito.it.lab2.entities.Student;
import ai.polito.it.lab2.entities.Team;
import ai.polito.it.lab2.repositories.CourseRepository;
import ai.polito.it.lab2.repositories.StudentRepository;
import ai.polito.it.lab2.repositories.TeamRepository;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TeamServicesImpl implements TeamServices {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    TeamRepository teamRepository;

    @Override
    public boolean addCourse(CourseDTO course) {
        if(courseRepository.existsById(course.getName())){
            return false;
        }
        if(course.getMax()>=course.getMin()) {
            courseRepository.save(modelMapper.map(course, Course.class));
            return true;
        }
        else return false;
    }

    @Override
    public Optional<CourseDTO> getCourse(String name) {
        return courseRepository.findById(name)
                .map(c -> modelMapper.map(c, CourseDTO.class));
    }

    @Override
    public List<CourseDTO> getAllCourses() {
        return courseRepository.findAll()
                .stream()
                .map(c -> modelMapper.map(c, CourseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean addStudent(StudentDTO student) {
        if(studentRepository.existsById(student.getId())){
            return false;
        }
        studentRepository.save(modelMapper.map(student, Student.class));
        return true;
    }

    @Override
    public Optional<StudentDTO> getStudent(String studentId) {
            return studentRepository.findById(studentId)
                    .map(s -> modelMapper.map(s, StudentDTO.class));
    }

    @Override
    public List<StudentDTO> getAllStudents() {
        return studentRepository.findAll()
                .stream()
                .map(s -> modelMapper.map(s, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentDTO> getEnrolledStudents(String courseName) {
        if(!courseRepository.existsById(courseName)){
            System.out.println("new exception in getEnrolledStudents");
            throw new CourseNotFoundException();
        }
        return courseRepository.getOne(courseName).getStudents()
                .stream()
                .map(student -> modelMapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public boolean addStudentToCourse(String studentId, String courseName) {
        if(!courseRepository.existsById(courseName)){
            System.out.println("new exception in addStudentToCourse: ");
            throw new CourseNotFoundException();
        }
        if(!studentRepository.existsById(studentId)){
            System.out.println("new exception in addStudentToCourse: ");
            throw new StudentNotFoundException();
        }

        Student student = studentRepository.getOne(studentId);
        Course course = courseRepository.getOne(courseName);
        if(!course.getStudents().contains(student) && course.isEnabled()) {
            course.addStudent(student);
            return true;
        }
        else return false;
    }

    @Override
    public void enableCourse(String courseName) {
        if(!courseRepository.existsById(courseName)){
            System.out.println("new exception in enableCourse");
            throw new CourseNotFoundException();
        }
        courseRepository.getOne(courseName).setEnabled(true);
    }

    @Override
    public void disableCourse(String courseName) {
        if(!courseRepository.existsById(courseName)){
            System.out.println("new exception in disableCourse");
            throw new CourseNotFoundException();
        }
        courseRepository.getOne(courseName).setEnabled(false);
    }

    @Override
    public List<Boolean> addAll(List<StudentDTO> students) {
        List<Boolean> booleans = new ArrayList<>();
        for (StudentDTO studentDTO: students) {
            if(addStudent(studentDTO)){
                booleans.add(true);
            }else booleans.add(false);
        }
        return booleans;
    }

    @Override
    public List<Boolean> enrollAll(List<String> studentIds, String courseName) {
        List<Boolean> booleans = new ArrayList<>();

        for(String studentId : studentIds){
            if(addStudentToCourse(studentId, courseName))
                booleans.add(true);
            else booleans.add(false);
        }
        return booleans;
    }

    @Override
    public List<Boolean> addAndEnroll(Reader r, String courseName) {
        List<Boolean> booleans1 = new ArrayList<>();
        List<Boolean> booleans2 = new ArrayList<>();
        List<Boolean> booleans = new ArrayList<>();

        CsvToBean<StudentDTO> csvToBean = new CsvToBeanBuilder<StudentDTO>(r)
                .withType(StudentDTO.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();
        List<StudentDTO> studentDTOS = csvToBean.parse();


        booleans1.addAll(addAll(studentDTOS));
        booleans2.addAll(enrollAll(studentDTOS.stream()
                .map(studentDTO -> studentDTO.getId())
                .collect(Collectors.toList()), courseName));

        for(int i=0; i<Math.max(booleans1.size(), booleans2.size()); i++) { //la dimensione dovrebbe comunque esser uguale
            if(booleans1.get(i) == true || booleans2.get(i) == true)
                booleans.add(true);
            else booleans.add(false);
        }
        return booleans;
    }

    @Override
    public List<CourseDTO> getCourses(String studentId) {
        if(!studentRepository.existsById(studentId))
            throw new StudentNotFoundException();
        Student student = studentRepository.getOne(studentId);
        return student.getCourses()
                .stream()
                .map(course -> modelMapper.map(course, CourseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<TeamDTO> getTeamsForStudent(String studentId) {
        if(!studentRepository.existsById(studentId))
            throw new StudentNotFoundException();
        return studentRepository.getOne(studentId).getTeams()
                .stream()
                .map(team -> modelMapper.map(team, TeamDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentDTO> getMembers(Long TeamId) {
        if(!teamRepository.existsById(TeamId))
            throw new TeamServicesException();
        return teamRepository.getOne(TeamId).getMembers()
                .stream()
                .map(student -> modelMapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public TeamDTO proposeTeam(String courseId, String name, List<String> membersId) {
        if(!courseRepository.existsById(courseId))
            throw new CourseNotFoundException();
        Course course = courseRepository.getOne(courseId);

        if(course.getMax()<membersId.size() || course.getMin()>membersId.size())
                throw new CourseCardinalityException();

        if(!course.isEnabled())
            throw new CourseEnabledException();

        if(findDuplicates(membersId))
            throw new StudentDuplicatedException();

        Team team = new Team();
        team.setName(name);

        for (String studentId : membersId) {
            if(!studentRepository.existsById(studentId))
                throw new StudentNotFoundException();
            Student student = studentRepository.getOne(studentId);
            if(!student.getCourses().contains(course))
                throw new StudentNotEnrolledException();
            List<Course> courses = student.getTeams()
                    .stream()
                    .map(t -> t.getCourse())
                    .filter(course1 -> course1.getName().equals(courseId))
                    .collect(Collectors.toList());
            if(courses.size()!=0)
                throw new StudentAlreadyInTeamException();
            team.setStatus(membersId.size());
            team.addStudentToTeam(student);
        }

        team.setCourse(course);
        teamRepository.save(team);

        return modelMapper.map(team, TeamDTO.class);
    }

    @Override
    public List<TeamDTO> getTeamForCourse(String courseName) {
        if(!courseRepository.existsById(courseName))
            throw new CourseNotFoundException();
        return courseRepository.getOne(courseName).getTeams()
                .stream()
                .map(team -> modelMapper.map(team, TeamDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentDTO> getStudentsInTeams(String courseName) {
        if(!courseRepository.existsById(courseName))
            throw new CourseNotFoundException();

        return courseRepository.getStudentsInTeams(courseName)
                .stream()
                .map(student -> modelMapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getStudentsNotInTeams(String courseName) {
        if(!courseRepository.existsById(courseName))
            throw new CourseNotFoundException();
        return courseRepository.getStudentsNotInTeams(courseName);
    }

    @Override
    public List<StudentDTO> getAvailableStudents(String courseName) {
        if(!courseRepository.existsById(courseName))
            throw new CourseNotFoundException();
        return courseRepository.getStudentsNotInTeams(courseName)
                .stream()
                .map(student -> modelMapper.map(student, StudentDTO.class))
                .collect(Collectors.toList());
    }

    boolean findDuplicates(List<String> ids) {
        final Set<String> set = new HashSet<String>();
        for (String string : ids) {
            if (!set.add(string))
                return true;
        }
        return false;
    }

}

