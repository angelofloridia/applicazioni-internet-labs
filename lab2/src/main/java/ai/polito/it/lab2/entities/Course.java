package ai.polito.it.lab2.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Course {
    @Id String name;
    int min;
    int max;
    boolean enabled;

    @ManyToMany(mappedBy = "courses")
    List<Student> students = new ArrayList<>();

    @OneToMany(mappedBy = "course")
    List<Team> teams = new ArrayList<>();

    public void addStudent(Student student){
        this.students.add(student);
        student.getCourses().add(this);
    }

    public Course addTeamToCourse(Team team){
        this.teams.add(team);
        team.getCourse().setName(this.getName());
        return this;
    }

    public void removeTeamFromCourse(Team team){
        this.teams.remove(team);
        team.getCourse().getTeams().remove(this);
    }
}
