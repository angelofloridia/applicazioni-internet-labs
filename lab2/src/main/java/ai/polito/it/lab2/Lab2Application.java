package ai.polito.it.lab2;

import ai.polito.it.lab2.dtos.CourseDTO;
import ai.polito.it.lab2.dtos.StudentDTO;
import ai.polito.it.lab2.dtos.TeamDTO;
import ai.polito.it.lab2.services.TeamServices;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Lab2Application {

//    @Bean
//    CommandLineRunner runner(CourseRepository courseRepository, StudentRepository studentRepository){
//        return new CommandLineRunner() {
//            @Override
//            public void run(String... args) throws Exception {
//                for(Course course : courseRepository.findAll()) System.out.println(course.toString());
//                for(Student student : studentRepository.findAll()) System.out.println(student.toString());
//
//            }
//        };
//    }

    @Bean
    CommandLineRunner runner(TeamServices teamServices){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
//                CourseDTO.CourseDTOBuilder builder = CourseDTO.builder();
//                builder.name("SWNET");
//                builder.min(1);
//                builder.max(6);
//                builder.enabled(true);
//                CourseDTO courseDTO = builder
//                        .build();
//                if(teamServices.addCourse(courseDTO))
//                    System.out.println(courseDTO.getName() + " Corso " + courseDTO.getName() + "added");
//                else System.out.println(courseDTO.getName()+ " not added, already present?");
//
//                CourseDTO courseDTO1 = CourseDTO.builder()
//                    .name("AI")
//                    .min(3)
//                    .max(4)
//                    .enabled(true)
//                    .build();
//                if(teamServices.addCourse(courseDTO1))
//                    System.out.println(courseDTO.getName() + " Corso " + courseDTO1.getName() + "added");
//                else System.out.println(courseDTO.getName()+ " not added, already present?");
//
//                System.out.println("AI: " + teamServices.getCourse("AI").toString());
//                System.out.println("TUTTI corsi: " + teamServices.getAllCourses().toString());
//
//
//                StudentDTO studentDTO = StudentDTO.builder()
//                        .id("1")
//                        .firstName("Angelo")
//                        .name("Floridia")
//                        .build();
//                if(teamServices.addStudent(studentDTO))
//                    System.out.println("Student " + studentDTO.getName() + "added!");
//                else System.out.println("Student not added");
//
//                System.out.println("Angelo: " + teamServices.getStudent("1").toString());
//                System.out.println("TUTTI studenti: " + teamServices.getAllStudents().toString());
//
//                if(teamServices.addStudentToCourse("1", "AI"))
//                    System.out.println("Student 1 added to AI");
//                teamServices.addStudentToCourse("1", "SWNET");
//
//                System.out.println("Corsi dello studente: " + teamServices.getCourses("1"));
//
//                teamServices.disableCourse("AI");
//                teamServices.enableCourse("AI");
//
//                File file = new File("/home/pepos/IdeaProjects/lab2/studenti.csv");
//                Reader reader = new BufferedReader(new FileReader(file));
//                teamServices.addAndEnroll(reader, "AI");
//
//                System.out.println("TUTTI gli studenti di AI: " + teamServices.getEnrolledStudents("AI").toString());
//
//                List<String> membersId = new ArrayList<>();
//                membersId.add("1748"); membersId.add("1950"); membersId.add("1968");
//                TeamDTO team = teamServices.proposeTeam("AI", "test", membersId);
//
//                System.out.println(team.toString());
//
//                System.out.println(teamServices.getTeamsForStudent("1748").toString());
//
//                System.out.println(teamServices.getMembers(team.getId()).toString());
//
//                System.out.println(teamServices.getTeamForCourse("AI"));
//
//                System.out.println(teamServices.getStudentsInTeams("AI").toString());
                System.out.println(teamServices.getAvailableStudents("AI").toString());
            }
        };
    }

    @Bean
    ModelMapper modelMapper(){
        return new ModelMapper();
    }

    public static void main(String[] args) {
        SpringApplication.run(Lab2Application.class, args);
    }
}
