package ai.polito.it.lab2.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Student {
    @Id
    String id;
    String name;
    String firstName;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "student_course",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_name"))
    List<Course> courses = new ArrayList<>();

    @ManyToMany(mappedBy = "members")
    List<Team> teams = new ArrayList<>();


    public void addCourse(Course course){
        this.courses.add(course);
        course.getStudents().add(this);
    }

    public void addTeamToStudent(Team team){
        this.teams.add(team);
        team.getMembers().add(this);
    }

    public void removeTeamFromStudent(Team team){
        this.teams.remove(team);
        team.getMembers().remove(this);
    }
}
