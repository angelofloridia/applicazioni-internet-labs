package ai.polito.it.lab2.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Team {
    @Id
    @GeneratedValue
    Long id;
    String name;
    int status;

    @ManyToOne
    @JoinColumn(name = "course_name")
    Course course;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "team_student",
            joinColumns = @JoinColumn(name = "team_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    List<Student> members = new ArrayList<>();

    public void setCourse(Course course) {
        if(course == null)
            unsetCourse();
        this.course=course;
        course.getTeams().add(this);
    }

    public void unsetCourse(){
        course.removeTeamFromCourse(this);
        this.course = null;
    }

    public void addStudentToTeam(Student student){
        if(student == null)
            removeStudentFromTeam(student);
        this.members.add(student);
        student.getTeams().add(this);
    }

    public void removeStudentFromTeam(Student student){
        student.removeTeamFromStudent(this);
        student.getTeams().remove(this);
    }
}
