package it.polito.ai.lab1;

import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value //privati e final
@Builder
public class RegistrationDetails {
    String  name;
    String  lastname;
    String  email;
    String  pwd;
    Boolean privacy;
    Date    registrationDate;
}

