package it.polito.ai.lab1;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegistrationCommand {
    @Size(min=1, max=50)         private String name;
    @Size(min=1, max=50)         private String lastname;
    @Email @Size(min=5, max=254) private String email;
    @Size(min=8, max=64)         private String pwd;
    @Size(min=8, max=64)         private String pwd2;
    @NotNull                     private Boolean privacy;
}
