package it.polito.ai.lab1;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;

@Controller
@Log(topic = "HomeController")
public class HomeController {

    @Autowired
    RegistrationManager registrationManager;

    @GetMapping("/")
    public String home(@ModelAttribute("command") LoginCommand loginCommand) {
        return "home";
    }

    @GetMapping("/register")
    public String registrationPage(@ModelAttribute("command") RegistrationCommand registrationCommand) {
        log.info("registrationPage " + registrationCommand);
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("command") RegistrationCommand registrationCommand, BindingResult bindingResult) {
        log.info("Post Register");
        log.severe("register " + registrationCommand);

        if (bindingResult.hasErrors())
            return "register";

        if (!registrationCommand.getPwd().equals(registrationCommand.getPwd2())) {
            bindingResult.addError(new FieldError("command", "pwd", "Passwords don't match!"));
            return "register";
        }

        if (!registrationCommand.getPrivacy()) {
            bindingResult.addError(new FieldError("command", "privacy", "You must accept the privacy policy!"));
        }

        if (bindingResult.hasErrors())
            return "register";

        RegistrationDetails rd = RegistrationDetails.builder().name(registrationCommand.getName())
                .lastname(registrationCommand.getLastname()).email(registrationCommand.getEmail()).pwd(registrationCommand.getPwd())
                .privacy(registrationCommand.getPrivacy()).registrationDate(new Date()).build();

        if (registrationManager.putIfAbsent(rd.getEmail(), rd) != null) {
            bindingResult.addError(new FieldError("command", "email", "This email is already present in database!"));
            return "register";
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }

        return "redirect:/";
    }

    @PostMapping("/login")
    public String login(@Valid @ModelAttribute("command") LoginCommand loginCommand, BindingResult bindingResult, HttpSession httpSession) {

        if (loginCommand.getEmail() == "") {
            bindingResult.addError(new FieldError("command", "email", "Insert a valid email address!"));
            return "home";
        }

        RegistrationDetails registrationDetails;
        if (registrationManager.get(loginCommand.getEmail()) != null)
            registrationDetails = registrationManager.get(loginCommand.getEmail());
        else {
            bindingResult.addError(new FieldError("command", "email", "Username or password wrong!"));
            return "home";
        }

        if (!registrationDetails.getPwd().equals(loginCommand.getPwd())) {
            bindingResult.addError(new FieldError("command", "email", "Username or password wrong!"));
            //lo metto uguale a quello di prima per ragioni di sicurezza
            return "home";
        }

        httpSession.setAttribute("username", loginCommand.getEmail());
        return "/private";
    }

    public String privatePage(HttpSession httpSession) {
        if (httpSession.getAttribute("username") == "")
            return "redirect: /";
        else
            return "private";

    }

    @PostMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute("username");
        return "redirect:/";
    }
}
